#include <iostream>
#include <vector>
#include <queue>

#define ll long long
#define boost cin.tie(nullptr); cout.tie(nullptr); ios::sync_with_stdio(false);

using namespace std;
using graph = vector< vector<ll> >;

ll num = 0;

ll dfs(ll u, const graph& gr, vector<ll>& visited, vector<ll>& answ) { // depth_first_search
  if (visited[u]) { return 0; }

  visited[u] = 1;
  answ[u] = num;
  ++num;
  ll count = 1;

  for(ll v: gr[u]) {
    count += dfs(v, gr, visited, answ);
  }
  
  return count;
}

int main() {
    boost
    ll m,n,k; // n - vertex
    cin >> n >> m >> k;
    k--;
    graph gr(n);

    for (ll i = 0; i < m; ++i) {
        ll u, v;
        cin >> u >> v;
        u -= 1;
        v -= 1;
        gr[u].push_back(v);
        gr[v].push_back(u);
    }

    vector<ll> visited;
    visited.assign(n, 0);
    vector<ll> answered;
    answered.assign(n, -1);
    dfs(k, gr, visited, answered);
    for (ll i = 0; i < answered.size(); ++i) {
        cout << answered[i] << " ";
    }

  return 0;
}
