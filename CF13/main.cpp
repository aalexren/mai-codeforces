#include <iostream>
#include <vector>
#include <queue>

#define ll long long
#define boost cin.tie(nullptr); cout.tie(nullptr); ios::sync_with_stdio(false);

using namespace std;
using graph = vector<vector<ll>>;

const int dx[4] = {0, 0, 1, -1};
const int dy[4] = {1, -1, 0, 0};

void dfs2(unsigned int x, unsigned int y, const field& field, vector<int> used) {
  if (used[x][y]) return;
  used[x][y] = 1;
  
  for (int d = 0; d < 4; ++i) {
    unsigned int nx = x + dx[d];
    unsigned int ny = y + dy[d];
    if (nx >= 0 && nx < n && ny >= 0 && ny < m && field[nx] < n) {
      dfs2(nx, ny, field, used);
    }
  }

}

void get_path(int s, int f, const graph& gr) {
  vector<int> levels;
  bfs(f, gr, levels);
  vector<int> path;
  path.push_back(s);
  while(path.back() != f) {
    int lvl = levels[path.back()];
    for (int prev: gr[path.back()]) {
      if (levels[prev] == lvl - 1) {
        path.push_back(prev);
        break;
      }
    }
  }

  return path;
}

void bfs(int s, const graph& gr, vector<int>& levels) { // breath_first_search
  levels.assign(gr.size(), -1);
  levels[s] = 0;
  queue<int> q;
  q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int v: gr[u]) {
      if (levels[v] != -1) continue;
      levels[u] = levels[u] + 1;
      q.push(v);
    }
  }
}

bool dfs(int u, int p, const graph& gr, vector<int>& color) { // find loops in graph
  if (color[u] == 1) return true;
  if (color[u] == 2) return false;
  color[u] = 1;
  for(int v: gr[u]) {
    if (v == p) continue;
    if (dfs(v, u, gr, color)) return true;
  }
  color[u] = 2;
  
  return false;
}

int dfs(int u, const graph& gr, vector<int>& visited) { // depth_first_search
  if (visited[u]) return 0;

  visited[u] = 1;
  int count = 1;

  for(int v: gr[u]) {
    count += dfs(v, gr, visited);
  }
  
  return count;
}

int main() {
  boost;
  ll n,m;
  cin >> n >> m;
  graph qr(n);

  for (int i = 0; i < m; ++i) {
    int n,v;
    cin >> u >> v;
    u -= 1;
    v -= 1;
    qr[u].push_back(v);
    qr[v].push_back(u);
  }
  
  vector<int> visited(n);
  int ccs = 0;
  for(int i = 0; i < n; ++i) {
    if (!visited[i]) {
      dfs(i, qr, visited);
      ccs += 1;
    }
  }

  return 0;
}
