#include <iostream>
#include <vector>
#include <queue>

#define ll long long
#define boost cin.tie(nullptr); cout.tie(nullptr); ios::sync_with_stdio(false);

using namespace std;
using graph = vector< vector<ll> >;

void bfs(ll s, const graph& gr, vector<ll>& levels) { // breath_first_search
  levels.assign(gr.size(), -1);
  levels[s] = 0;
  queue<ll> q;
  q.push(s);
  while (!q.empty()) {
    ll u = q.front();
    q.pop();
    for (ll v: gr[u]) {
      if (levels[v] != -1) continue;
      levels[u] = levels[u] + 1;
      q.push(v);
    }
  }
}

ll get_path(ll s, ll f, const graph& gr) {
  vector<ll> levels;
  bfs(f, gr, levels);
  vector<ll> path;
  path.push_back(s);
  while(path.back() != f) {
    ll lvl = levels[path.back()];
    for (ll prev: gr[path.back()]) {
      if (levels[prev] == lvl - 1) {
        path.push_back(prev);
        break;
      }
    }
  }

  return path.size();
}

int main() {
    boost
    ll m, n, k, s; // n - vertex
    cin >> n >> m >> k;
    graph gr(n);
    k--;

    for (ll i = 0; i < m; ++i) {
        ll u, v;
        cin >> u >> v;
        u -= 1;
        v -= 1;
        gr[u].push_back(v);
        gr[v].push_back(u);
    }

    for(int i = 0; i < n; ++i){
        s = get_path(i, k, gr);
        if (s == -1){cout << s << ' ';} else{
        cout << s - 1 << ' ';}
    }
    // vector<ll> levels;
    // //bfs(0, gr, levels, k);
    // auto res = get_path(0, k, gr);
    // for (ll i = 0; i < res.size(); ++i) {
    //     cout << res[i] << " ";// << " " << levels[i] << " ";
    // }

  return 0;
}
