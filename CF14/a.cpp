#include<iostream> //sssp single source short path problem
#include<vector>
 
using namespace std;
 
#define ll long long
 
struct Edge{
    ll u, v, w;
};
 
int main(){
    ll n, m, s;
    cin >> n >> m >> s;
    vector<Edge> edges(m);
    for (int i = 0; i < m; ++i){
      ll u, v, w; cin >> u >> v >> w;
      edges[i].u = u;
      edges[i].v = v;
      edges[i].w = w;
    }
    vector<ll> distances(n, INT16_MAX);
    vector<ll> parents(n, -1);
    distances[s] = 0;
    for (int i = 0; i < n-1; ++i){
        bool changed = false;
        for (const Edge& e:edges){
	  if (distances[e.v] > distances[e.u] + e.w) {distances[e.v] = distances[e.u] + e.w; changed = true; parents[e.v] = e.u;}
	  if (distances[e.u] > distances[e.v] + e.w) {distances[e.u] = distances[e.v] + e.w; changed = true; parents[e.u] = e.v;}
        }
    }

    for (int i = 0; i < n; ++i) {
      cout << distances[i] << " ";
    }
    
    return 0;
   /* for (int i = 1; i < n; ++i){
        vector<ll> tmp = distances;
        for (const Edge& e:edges){
            if (tmp[e.v] > tmp[e.u] + e.w) tmp[e.v] = tmp[e.u] + e.w;
            if (tmp[e.u] > tmp[e.v] + e.w) tmp[e.u] = tmp[e.v] + e.w;
        }
        distances = tmp;
    }*/
}
