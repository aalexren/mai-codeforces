#include <iostream>
#include <vector>

using namespace std;

#define ll long long

const int INF = 1000000000;
 
int main() {
	ll n;
	cin >> n;
	vector<vector<pair<ll,ll> > > g(n);
  for (ll i = 0; i < n; ++i) {
    for (ll j = 0; j < n; ++j) {
      cin >> g[i][j].first;
    }
  }
	long long s;
  cin >> s;
 
	vector<ll> d (n, INF),  p (n);
	d[s] = 0;
	vector<char> u (n);
	for (ll i=0; i<n; ++i) {
		ll v = -1;
		for (ll j=0; j<n; ++j)
			if (!u[j] && (v == -1 || d[j] < d[v]))
				v = j;
		if (d[v] == INF)
			break;
		u[v] = true;
 
		for (ll j=0; j<g[v].size(); ++j) {
			ll to = g[v][j].first,
				len = g[v][j].second;
			if (d[v] + len < d[to]) {
				d[to] = d[v] + len;
				p[to] = v;
			}
		}
	}

  for (ll i = 0; i < n; ++i) {
    cout << d[i];
  }
}
