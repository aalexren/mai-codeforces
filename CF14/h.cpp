#include<iostream>
#include<vector>
 
using namespace std;
 
#define ll long long
 
int main(){
    ll n;
    cin >> n;
    vector< vector<ll> > distances(n, vector<ll>(n));
    for (int i = 0; i < n; ++i){
        for (int j = 0; j < n; ++j){
            cin >> distances[i][j];
        }
    }

    for(int k = 0; k < n; ++k){
        for(int i = 0; i < n; ++i){
            for (int j = 0; j < n; ++j){
                if (distances[i][j] > distances[i][k] + distances[k][j]) {
                    distances[i][j] = distances[i][k] + distances[k][j];
                }
            }
        }
    }

    for (int i = 0; i < n; ++i){
        for (int j = 0; j < n; ++j){
            cout << distances[i][j] << " ";
        }
        cout << "\n";
    }
}
