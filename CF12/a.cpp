#include <iostream>
#include <vector>

#define ll long long
using namespace std;

ll gcd (ll a, ll b) {
	return b ? gcd (b, a % b) : a;
}

struct segtree {
    ll n;
    vector < ll > data;
    ll get(ll l, ll r) {
        return get(0, 0, n - 1, l, r);
    }
    ll set(ll p, ll x) {
        set(0, 0, n - 1, p, x);
    }
    ll get(ll id, ll sl, ll sr, ll ql, ll qr) {
        if (sl >= ql && sr <= qr) {
            return data[id];
        }
        ll m = (sl + sr) / 2;
        if (qr <= m) {
            return get(id * 2 + 1, sl, m, ql, qr);
        } else if (ql > m) {
            return get(id * 2 + 2, m + 1, sr, ql, qr);
        } else {
            return get(id * 2 + 1, sl, m, ql, qr) + get(id * 2 + 2, m + 1, sr, ql, qr);
        }
    }
    // void set(int id, int sl, int sr, int p, int x){ 
    //     if (sl == sr){ 
    //     data[id] += x; 
    //     return; 
    //     } 
    //     int m = (sl + sr)/2; 
    //     if (p <= m){set(id*2 + 1, sl, m, p, x);} 
    //     else {set(id*2+2, m+1, sr, p, x);} 
    //     data[id]=data[id*2+1] + data[id*2+2]; 
    // }
    void set(ll id, ll sl, ll sr, ll p, ll x) {
        if (sl == sr) {
            data[id] += x;
            return;
        }
        ll m = (sl + sr) / 2;
        if (m >= p) {
            set(id * 2 + 2, sl, m + 1, p, x);
        } else {
            set(id * 2 + 1, m, sr, p, x);
        }
        data[id] = data[id * 2 + 1] + data[id * 2 + 2];
    }
    segtree(ll n_) {
        n = n_;
        data.resize(n * 4);
    }
    void build(const vector < ll > & a) {
        build(0, 0, n - 1, a);
    }
    void build(ll id, ll sl, ll sr,
        const vector < ll > & a) {
        if (sl == sr) {
            data[id] = a[sl];
            return;
        }
        ll m = (sl + sr) / 2;
        build(id * 2 + 1, sl, m, a);
        build(id * 2 + 2, m + 1, sr, a);
        data[id] = data[id * 2 + 1] + data[id * 2 + 2];
    }
};
 
// struct segtree {
//     ll n;
//     vector < ll > data;
//     ll get(ll l, ll r, ll p) {
//         return get(0, 0, n - 1, l, r, ll p);
//     }
//     ll set(ll p, ll x) {
//         set(0, 0, n - 1, p, x);
//     }
//     ll get(ll id, ll sl, ll sr, ll ql, ll qr, ll p) {
//         if (sl == sr) {
//             return data[id];
//         }
//         ll m = (sl + sr) / 2;
//         if (p <= m) {
//             return get(id * 2 + 1, sl, m, ql, qr, p) + data[id];
//         } else {
//             return get(id * 2 + 2, m + 1, sr, ql, qr, p) + data[id];
//         }
//     }
//     void add(ll id, ll sl, ll sr, ll ql, ll qr, ll x) {
//         if (ql <= sl && sr <= qr) {
//             data[id] += x;
//             return;
//         }
//         ll m = (sl + sr) / 2;
//         if (ql <= m) {
//             add(id * 2 + 1, sl, m, ql, qr, x);
//         }
//         if (m < qr) {
//             add(id * 2 + 2, m + 1, sr, ql, qr, x);
//         }
//     }
//     void set(ll id, ll sl, ll sr, ll p, ll x) {
//         if (sl == sr) {
//             data[id] += x;
//             return;
//         }
//         ll m = (sl + sr) / 2;
//         if (m >= p) {
//             set(id * 2 + 1, m, sr, p, x);
//         } else {
//             set(id * 2 + 2, sl, m + 1, p, x);
//         }
//         data[id] = data[id * 2 + 1] + data[id * 2 + 2];
//     }
//     segtree (ll n_) {
//         n = n_;
//         data.resize(n * 4);
//     }
//     void build(const vector < ll > & a) {
//         build(0, 0, n - 1, a);
//     }
//     void build(ll id, ll sl, ll sr,
//         const vector < ll > & a) {
//         if (sl == sr) {
//             data[id] = a[sl];
//             return;
//         }
//         ll m = (sl + sr) / 2;
//         build(id * 2 + 1, sl, m, a);
//         build(id * 2 + 2, m + 1, sr, a);
//         data[id] = data[id * 2 + 1] + data[id * 2 + 2];
//     }
// };

// struct SegmentTree{
//     ll get(ll l, ll r){
//         return get(0,0,n-1,l,r);
//     }
//     ll set(ll p, ll x){
//         set(0,0,n-1,p,x);
//     }
//     ll get(ll id, ll sl, ll sr, ll ql, ll qr){
//         if (ql <= sl && sr <= qr){
//             return data[id];
//         }
//         ll m = (sl + sr)/2; //???
//         if (qr <= m){return get(id*2 + 1, sl, m, ql, qr);}
//         if (m < ql){return get(id*2 + 2, m+1, sr, ql, qr);}
//         return max(get(id*2+1, sl, m, ql, qr), get(id*2 + 2, m + 1, sr, qr, ql));
//     }
//     void set(ll id, ll sl, ll sr, ll p, ll x){
//         if (sl == sr){
//             data[id] += x;
//             return;
//         }
//         ll m = (sl + sr)/2;
//         if (p <= m){set(id*2 + 1, sl, m, p, x);}
//         else {set(id*2+2, m+1, sr, p, x);}
//         data[id] = data[id*2+1] + data[id*2+2];
//     }
//     ll n;
//     vector<ll> data;
//     SegmentTree (ll n_){ // V dereve ili net?
//         n = n_;
//         data.resize(n*4);
//     }
//     void build(const vector<ll>& a){
//         build(0, 0, n-1, a);
//     }
//     void build(ll id, ll sl, ll sr, const vector<ll>& a){
//         if (sl == sr){
//             data[id] = a[sl];
//             return;
//         }
//         ll m = (sl + sr)/2;
//         build(id*2+1, sl, m, a);
//         build(id*2+2, m+1, sr, a);
//         data[id] = max(data[id*2+1], data[id*2+2]);
//     }
// };

int main() {

    ll n; cin >> n;
    vector<ll> data(n);
    //SegmentTree st(n);
    segtree st(n);
    for (ll i = 0; i < n; i++) {
        cin >> data[i];
    }
    st.build(data);
    ll q; cin >> q;
    ll l, r, action;
    for (ll i = 0; i < q; i++) {
        cin >> action >> l >> r;
        if (!action) st.set(l, r);
        else if (action) cout << st.get(l-1, r-1) << "\n";
    }

    return 0;
}