#include<iostream> //Derevo otrezkov sait e-maxx.ru/algo sait s infoy
#include<vector>
#define boost cin.tie(nullptr); cout.tie(nullptr); ios::sync_with_stdio(false);
using namespace std;
 #define ll long long 

struct SegmentTree {
    vector<int> t;
    int n;
    SegmentTree(ll n_){
        n = n_;
        data.resize(n*4);
    }
void build (const & vector<int> a, int v, int tl, int tr) {
	if (tl == tr)
		t[v] = a[tl];
	else {
		int tm = (tl + tr) / 2;
		build (a, v*2, tl, tm);
		build (a, v*2+1, tm+1, tr);
		t[v] = t[v*2] + t[v*2+1];
	}
}
int sum (int v, int tl, int tr, int l, int r) {
	if (l > r)
		return 0;
	if (l == tl && r == tr)
		return t[v];
	int tm = (tl + tr) / 2;
	return sum (v*2, tl, tm, l, min(r,tm))
		+ sum (v*2+1, tm+1, tr, max(l,tm+1), r);
}
void update (int v, int tl, int tr, int pos, int new_val) {
	if (tl == tr)
		t[v] = new_val;
	else {
		int tm = (tl + tr) / 2;
		if (pos <= tm)
			update (v*2, tl, tm, pos, new_val);
		else
			update (v*2+1, tm+1, tr, pos, new_val);
		t[v] = t[v*2] + t[v*2+1];
	}
}
};

// struct SegmentTree{
//     ll get(ll l, ll r){
//         return get(0,0,n-1,l,r);
//     }
//     void set(ll p, ll x){
//         set(0,0,n-1,p,x);
//     }
//     ll get(ll id, ll sl, ll sr, ll ql, ll qr) {
//         if (ql <= sl && sr <= qr) {
//             return data[id];
//         }
//         ll m = (sl + sr) / 2;
//         if (qr <= m) {
//             return get(id * 2 + 1, sl, m, ql, qr);
//         }
//         if (ql > m) {
//             return get(id * 2 + 2, m + 1, sr, ql, qr);
//         }
//         return get(id * 2 + 1, sl, m, ql, qr) + get(id * 2 + 2, m + 1, sr, ql, qr);
//     }
//     void set(ll id, ll sl, ll sr, ll p, ll x){
//         if (sl == sr){
//             data[id] += x;
//             return;
//         }
//         ll m = (sl + sr)/2;
//         if (p <= m){set(id*2 + 1, sl, m, p, x);}
//         else {set(id*2+2, m+1, sr, p, x);}
//         data[id]=data[id*2+1] + data[id*2+2];
//     }
//     ll n;
//     vector<ll> data;
//     SegmentTree(ll n_){
//         n = n_;
//         data.resize(n*4);
//     }
//     void build(const vector<ll>& a){
//         build(0, 0, n-1, a);
//     }
//     void build(ll id, ll sl, ll sr, const vector<ll>& a){
//         if (sl == sr){
//             data[id] = a[sl];
//             return;
//         }
//         ll m = (sl + sr)/2;
//         build(id*2+1, sl, m, a);
//         build(id*2+2, m + 1, sr, a);
//         data[id] = data[id*2+1] + data[id*2+2];
//     }
// };
 
int main(){
    boost;
    ll n, k, q, x, y;
    cin >> n;
    SegmentTree s(n);
    vector<ll> a(n);
    for (ll i = 0; i < n; ++i){
        cin >> a[i];  
    }
    cin >> k;
    s.build(a);
    for (ll i = 0; i < n; ++i){
        cin >> q >> x >> y;
        if (q == 0){
          cout << s.get(x-1,y-1) << "\n";
        }
        else{
            s.set(x-1, y);
        }
    }
}