#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <set>
#include <ctime>

using namespace std;
using ll = long long;
#define bst cin.tie(nullptr); cout.tie(nullptr); ios_base::sync_with_stdio(false);

int main() {
  bst;

  int n; cin >> n;
  vector<ll> nums(n);

  ll sum = 0;
  for(int i = 0; i < n; ++i) {
    cin >> nums[i];
  }

  ll maxx = nums[0];
  for(int i = 0; i < n; ++i) {
    maxx = nums[i];
    for(int j = i; j < n; ++j) {
      maxx = max(maxx, nums[j]);
      sum += maxx;
    }
  }
  
  cout << sum;

  return 0;
}
