# Деревья отрезков

Задача для $10^{18}$ элементов и больше. Создаем только для тех узлов, где что-то есть.

```c++
#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <set>
#include <ctime>

using namespace std;
using ll = long long;
#define bst cin.tie(nullptr); cout.tie(nullptr); ios_base::sync_with_stdio(false);

// хранит детей
struct Node {
  Node* left = nullptr;
  Node* right = nullptr;
  int data; // для поиска максимума и добавления значений
};

struct SegmentTree {
  int n; // размер ячеек памяти (типа размер массива)
  Node* root = nullptr;
  
  SegmentTree(int n_) {
    this->n = n_;
  }
  
  void get(int l, int r) {
    get(root, 0, n - 1, l, r);
  }
  
  int get(Node* node, int sl, int sr, int ql, int qr) {
    // ql - query left, qr - query right
    if (node = nullptr) return 0; // ответ по умолчанию, если здесь ничего не меняли
      
    if (ql <= sl && sr <= qr) {
      return node_data(node); // !нельзя писать node->data
    }
    int m = (sl + sr)/2;
    if (qr <= m) {
      return get(node->left, sl, m, ql, qr);
    }
    if (m < ql) {
      return get(node->right, m + 1, sr, ql, qr);
    }
    return max(get(node->left, sl, m, ql, qr), get(node->right, m + 1, sr, ql, qr));
  }
  
  void set(int p, int x) {
    // p - position, x - value
    root = set(root, 0, n-1, p, x);
  }
  
  int node_data(Node* node) {
    if (node == nullptr) return 0;
    return node->data;
  }
  
  Node* set(Node* node, int sl, int sr, int p, int x) {
  // устанавливает в какую-то позицию какое-то значение
  // sr - правая граница, sl - левая граница
  	if (node == nullptr) {
    	node = new Node; // node = malloc(sizeof(Node))
    }
    
    if (sl == sr) { // дошли до конечного узла, обновляем значение
      node->data = x;
      return node;
    }
  	int m = (sl + sr)/2;
  	if (p <= m) {
      node->left = set(node->left, sl, m, p, x);
    }
  	else {
      node->right = set(node->right, m+1, sr, p, x);
    }
    node->data = max(node_data(node->left), node_data(node->right));
    return node;
    //node->data = max(node->left->data, node->right->data);
  }
};

int main() {
  bst;
  
  return 0;
}
```

Изначально массив равняется нулю. Нет "билда". В худшем случае памяти будет занимать $O(q*log_n),$ $q$ – количество запросов.

------

```c++
struct SegmentTree {
  int n;
  vector<int> data;
  vector<int> delay; // вектор с отложенными обновлениями
  
  //build надо написать, задаем n и вектора размера 4n
  
  void push(int id, int sl, int sr) {
    data[id] += delay[id];
    if (sl != sr) {
      delay[id*2+1] += delay[id]; // что детям нужно будет 
      delay[id*2+2] += delay[id]; // передать в дальнейшем
    }
    delay[id] = 0; // ничего отложенного больше не осталось
  }
  
  int get(int id, int sl, int sr, int ql, int qr) {
    push(id, sl, sr);
    if (ql <= sl && sr <= qr) {
      return data[id];
    }
    int m = (sl + sr)/2;
    if (qr <= m) {
      return get(id*2+1, sl, m, ql, qr);
    }
    if(m < ql) {
      return get(id*2+2, m+1, sr, ql, qr);
    }
    return max(get(id*2+2, m+1, sr, ql, qr), get(id*2+1, sl, m, ql, qr));
  }
  
  void add(int id, int sl, int sr, int ql, int qr, int x) {
    // ql, qr - positions, x - value to add
    if (ql <= sl && sr <= qr) {
      delay[id] += x;
      return;
    }
    push(id, sl, sr); // обновляем всё, что прошли. В узле не осталось ничего, что нам нужно когда-то будет обновить.
  
  	int m = (sl + sr)/2;
    if (ql <= m) {
      add(id*2+1, sl, m, ql, qr, x);
    }
    if(m < qr) {
      add(id*2+2, m+1, sr, ql, qr, x);
    }
    
    data[id] = max(get(id*2+1, sl, m, sl, m), get(id*2+2, m+1, sr, m+1, sr));
  }
}
```



```bash
g++ -g3 a.cpp
```

ключ `-g3` указывает сколько передать информации дебаггеру.

```bash
valgrind ./a.out
```

`valgrind` покажет ошибки с памятью в программе. Можно воспользоваться и стандартным `gdb`.

------

```bash
g++ -E -dD -xc++ /dev/null
```

выводит предупреждения и пр.