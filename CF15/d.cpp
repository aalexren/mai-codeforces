#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <set>
#include <ctime>

using namespace std;
using ll = long long;
#define bst cin.tie(nullptr); cout.tie(nullptr); ios_base::sync_with_stdio(false);

// хранит детей
struct Node {
  Node* left = nullptr;
  Node* right = nullptr;
  ll data; // для поиска максимума и добавления значений
};

struct SegmentTree {
  ll n; // размер ячеек памяти (типа размер массива)
  Node* root = nullptr;
  
  SegmentTree(ll n_) {
    this->n = n_;
  }
  
  ll get(ll l, ll r) {
    return get(root, 0, n - 1, l, r);
  }
  
  ll get(Node* node, ll sl, ll sr, ll ql, ll qr) {
    // ql - query left, qr - query right
    if (node == nullptr) return 0; // ответ по умолчанию, если здесь ничего не меняли
      
    if (ql <= sl && sr <= qr) {
      return node_data(node); // !нельзя писать node->data
    }
    ll m = (sl + sr)/2;
    if (qr <= m) {
      return get(node->left, sl, m, ql, qr);
    }
    if (m < ql) {
      return get(node->right, m + 1, sr, ql, qr);
    }
    return get(node->left, sl, m, ql, qr) + get(node->right, m + 1, sr, ql, qr);
  }
  
  void set(ll p, ll x) {
    // p - position, x - value
    root = set(root, 0, n-1, p, x);
  }
  
  ll node_data(Node* node) {
    if (node == nullptr) return 0;
    return node->data;
  }
  
  Node* set(Node* node, ll sl, ll sr, ll p, ll x) {
  // устанавливает в какую-то позицию какое-то значение
  // sr - правая граница, sl - левая граница
  	if (node == nullptr) {
    	node = new Node; // node = malloc(sizeof(Node))
    }
    
    if (sl == sr) { // дошли до конечного узла, обновляем значение
      node->data = x;
      return node;
    }
  	ll m = (sl + sr)/2;
  	if (p <= m) {
      node->left = set(node->left, sl, m, p, x);
    }
  	else {
      node->right = set(node->right, m+1, sr, p, x);
    }
    node->data = node_data(node->left) + node_data(node->right);
    return node;
    //node->data = max(node->left->data, node->right->data);
  }
};

SegmentTree st(1e10);

int main() {
  bst;
  
  ll n; cin >> n;
  for(ll i = 0; i < n; ++i) {
      ll q,x,y;
      cin >> q >> x >> y;
      if (q) st.set(x, st.get(x,x)+y);
      else { y; cout << st.get(x, y) << "\n"; }
  }

  return 0;
}