#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <set>
#include <ctime>

using namespace std;
using ll = long long;
#define bst cin.tie(nullptr); cout.tie(nullptr); ios_base::sync_with_stdio(false);

struct SegmentTree {
  ll n;
  vector<ll> data;
  vector<ll> delay; // вектор с отложенными обновлениями
  
  SegmentTree(ll n_) {
    n = n_;
    data.resize(n * 6);
    delay.resize(n * 6);
  }

  void build(const vector < ll > & a) {
        build(0, 0, n - 1, a);
    }
  void build(ll id, ll sl, ll sr, const vector < ll > & a) {
    if (sl == sr) {
        data[id] = a[sl];
        return;
    }
    ll m = (sl + sr) / 2;
    build(id * 2 + 1, sl, m, a);
    build(id * 2 + 2, m + 1, sr, a);
    data[id] = min(data[id * 2 + 1], data[id * 2 + 2]);
  }
  //build надо написать, задаем n и вектора размера 4n
  
  void push(ll id, ll sl, ll sr) {
    data[id] += delay[id];
    if (sl != sr) {
      delay[id*2+1] += delay[id]; // что детям нужно будет 
      delay[id*2+2] += delay[id]; // передать в дальнейшем
    }
    delay[id] = 0; // ничего отложенного больше не осталось
  }
  
  ll get(ll l, ll r) {
    return get(0, 0, n - 1, l, r);
  }

  ll get(ll id, ll sl, ll sr, ll ql, ll qr) {
    push(id, sl, sr);
    if (ql <= sl && sr <= qr) {
      return data[id];
    }
    ll m = (sl + sr)/2;
    if (qr <= m) {
      return get(id*2+1, sl, m, ql, qr);
    }
    if(m < ql) {
      return get(id*2+2, m+1, sr, ql, qr);
    }
    return min(get(id*2+2, m+1, sr, ql, qr), get(id*2+1, sl, m, ql, qr));
  }
  
  void add(ll l, ll r, ll x) {
    add(0, 0, n-1, l, r, x);
  }

  void add(ll id, ll sl, ll sr, ll ql, ll qr, ll x) {
    // ql, qr - positions, x - value to add
    if (ql <= sl && sr <= qr) {
      delay[id] += x;
      return;
    }
    push(id, sl, sr); // обновляем всё, что прошли. В узле не осталось ничего, что нам нужно когда-то будет обновить.
  
  	ll m = (sl + sr)/2;
    if (ql <= m) {
      add(id*2+1, sl, m, ql, qr, x);
    }
    if(m < qr) {
      add(id*2+2, m+1, sr, ql, qr, x);
    }
    
    data[id] = max(get(id*2+1, sl, m, sl, m), get(id*2+2, m+1, sr, m+1, sr));
  }
};

int main() {
  bst;
  
  ll n;
  cin >> n;
  vector<ll> nums(n);
  SegmentTree st(n);
  for(ll i = 0; i < n; ++i) {
      cin >> nums[i];
  }
  st.build(nums);

  ll k; cin >> k;
  for(ll i = 0; i < k; ++i) {
      ll l, r, x;
      cin >> l >> r >> x;
      l--; r--;
      st.add(l, r, x);
      int m = st.get(l,r);
      if (m < x) cout << "None\n";
      else cout << m << "\n";
  }
  

  return 0;
}