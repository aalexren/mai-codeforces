#include <algorithm>
#include <set>
#include <stdio.h>
#include <vector>
#include <iostream>
  
using namespace std;
using ll = long long;
#define bst cin.tie(nullptr); cout.tie(nullptr); ios_base::sync_with_stdio(false);
  
struct dsu{
    vector<int> leader;
    vector<int> size;
  
    void make(int v) {
        leader[v] = v;
        size[v] = 1;
    }

    int find(int x){
        if(leader[x]==x) return x;
        leader[x]=find(leader[x]);
        return leader[x];
    }

    bool join(int x, int y){
        int xp=find(x);
        int yp=find(y);
        if(xp==yp) return false;
        if(size[xp]>size[yp]){
            leader[yp]=xp;
            size[xp]=size[xp]+size[yp];
        }else{
            leader[xp]=yp;
            size[yp]=size[xp]+size[yp];
        }
        return true;
    }
  
    dsu(int n){
        leader.resize(n);
        size.resize(n);
        for(int i = 0; i < n; ++i){
            leader[i]=i;
            size[i]=1;
        }
    }
};
  
struct edge{
    int u, v;
    long long w;
};
  
bool operator < (edge x, edge y){
    return x.w < y.w;
}
  
int main(){
    bst;
    int n, m, u, v;
    scanf("%d %d", &n, &m);
    dsu d(n);
    //vector<edge> g(n);

    for(int i = 0; i < m; i++){
        scanf("%d%d", &u, &v);
        d.join(--u, --v);
        // for (int j = 0; j < n; ++j)
        //     printf("%d ", d.size[j]);
        // printf("\n");
        printf("%d\n", d.size[d.find(u)]);
        //printf("%d\n", d.leader[d.find(u)]);
        //d.join(u, v);
       // printf("%d\n", d.size[u]);
        //if (d.join(u, v)) printf("%d\n", d.size[d.find(u)]);
        //else { d.make(u); d.make(v); d.join(u, v); printf("%d\n", d.size[d.find(u)]); }
        // for (int j = 0; j < n; ++j)
        //     printf("%d ", d.leader[j]);
        // printf("\n");
        //printf("%d\n", d.leader[u]);
        //scanf("%d%d", &u, &v);
    }
    
    return 0;
}