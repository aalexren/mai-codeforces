#include <stdio.h>

#include <algorithm>
#include <iostream>
#include <set>
#include <vector>

using namespace std;
using ll = long long;
#define bst           \
   cin.tie(nullptr);  \
   cout.tie(nullptr); \
   ios_base::sync_with_stdio(false);

struct sum {
   ll all;
   int min;
   int max;
}; 

struct dus {
   vector<int> leader;
   vector<int> size;  // not a rank, exactly size
   vector<sum> sums;

   void make(int i) {
      if (leader[i] == -1) {
         leader[i] = i;
         size[i] = 1;
         sums[i].all += i;
         sums[i].max = i;
         sums[i].min = i;
      }
      return;
   }

   int find(int i) {
      if (leader[i] != i) leader[i] = find(leader[i]);
      return leader[i];
   }

   void join(int x, int y) {
      int x_id = find(x);
      int y_id = find(y);

      /* already joined */
      if (x_id == y_id) return;

      if (size[x_id] > size[y_id]) {
         leader[y_id] = x_id;
         sums[x_id].all += sums[y_id].all;
         sums[x_id].min = min(sums[y_id].min, sums[x_id].min);
         sums[x_id].max = max(sums[y_id].max, sums[x_id].max);
      } 
      else {
         leader[x_id] = y_id;
         if (size[x_id] == size[y_id])
            size[y_id] += size[x_id];
         sums[y_id].all += sums[x_id].all;
         sums[y_id].min = min(sums[y_id].min, sums[x_id].min);
         sums[y_id].max = max(sums[y_id].max, sums[x_id].max);
      }

      return;
   }

   int getMax(int i) {
      return sums[find(i)].max;
   }

   int getMin(int i) {
      return sums[find(i)].min;
   }

   ll getSum(int i) {
      return sums[find(i)].all;
   }

   dus(int n) {
      n++;
      leader.assign(n, -1);
      size.assign(n, 1);
      sums.resize(n);
      for (int i = 0; i < n; ++i) {
         sums[i].all = 0;
         sums[i].min = INT32_MAX;
         sums[i].max = INT32_MIN;
      }
   }
};

int main() {
   bst;
   int n, m, u, v;
   scanf("%d %d", &n, &m);
   dus d(n);

   for (int i = 0; i < m; i++) {
      scanf("%d%d", &u, &v);
      d.make(u); d.make(v);
      d.join(u, v);
      printf("%lld ", d.getSum(u));
      printf("%d %d\n", d.getMin(v), d.getMax(u));
   }

   return 0;
}